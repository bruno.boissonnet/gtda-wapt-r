# WAPT-R

Paquet [WAPT](https://www.tranquil.it/gerer-parc-informatique/decouvrir-wapt/) pour l'installation silencieuse de RStudio.


## Paquet R

Ce paquet installe la dernière version de R de manière silencieuse.

- Dernière version : 4.3.1 (16/06/2023) 
- Sources de l’appli : https://cran.r-project.org/bin/windows/base/old/
- Paramètres d’installation silencieuse : /VERYSILENT /COMPONENTS="main,x64,translations" 
- Prérequis : Windows 10/11 
- Dépendances : Néant 
- Changlog : https://cran.r-project.org/doc/manuals/r-devel/NEWS.html
- Spécificités complémentaires d’installation : Supprimer l’icône du bureau public 
- Spécificités complémentaires en ce qui concerne les clés de registre : Néant 

*📌 NOTE : Depuis la version 4.2, le dossier personnel des bibliothèques se trouve dans le dossier `%LOCALAPPDATA%`.*

Personnalisation de l'installation : https://cran.r-project.org/bin/windows/base/rw-FAQ.html#Can-I-customize-the-installation_003f


## Utiliser ce paquet

Pour utiliser ce paquet, il faut : 

1. Télécharger tous les fichiers
2. Ouvrir la console WAPT
3. Générer un modèle de paquet
4. Pointer sur les fichiers téléchargés


## Notes de version

### 4.3.3

- Nouveau fichier d'installation : R-4.3.3-win.exe

#### FICHIER : control

- MODIFICATION : 
```
version           : 4.3.3
```

#### FICHIER : setup.py

- MODIFICATION : Généralisation du code pour ne plus avoir à le modifier à chaque nouvelle version.

### 4.3.1

- Nouveau fichier d'installation : R-4.3.1-win.exe

#### FICHIER : control

- MODIFICATION : 
```
version           : 4.3.1-1
```

#### FICHIER : setup.py

- MODIFICATION : Remplacement de 4.2.2 par 4.3.1 dans tout le fichier


### 4.2.2-1

- Renommage du projet : ae-R.4.2.2 en ae-R.4
- Généralisation du fichier exe : on récupère le fichier exe quelque soit son nom
- Suppression de l'icône du bureau : l'icône du bureau public est supprimée

